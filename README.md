# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

*Classifies Pokemon based on their images, using machine learning. It uses a Convolutional neural network combined with some 
Matlab trickery to get accurate results to find pokemon type. 

### How do I get set up? ###

* Just clone and pull the repo. 
* Run the the best code code segments to build models (bestcode_model.py and best_model_run.py)


### Contribution guidelines ###

*Talk to repo owner

### Who do I talk to? ###

* Repo owner